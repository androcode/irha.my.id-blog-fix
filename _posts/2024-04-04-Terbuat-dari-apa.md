---
title: 'Terbuat Dari Apa?'
layout: post
date: '2024-04-04 07:35:33'
tags:
- jekyll
categories:
- Jekyll
author: Irham
comments: true
image: /assets/img/IRHA.MY.ID.png
seo_description_max_words: 200
permalink: "/made-of-what/"
description : 'Ini terbuat dari rindu yg menggebu gebu, kecewa yang harus
rela dan juga sedikit Jekyll'
---
## [irha.my.id](https://irha.my.id) Terbuat dari apa?
Terbuat dari rindu yang menggebu gebu, dari sesal yang tak menemui asal,dari kecewa yang menjadikannya lara, wkwkwk bercanda biar ga kaku kaku amat. <br>
[irha.my.id](https://irha.my.id) terbuat sepenuhnya dari Jekyll dan sisa sisa rindu.
### Apa sih Jekyll itu?
Jekyll adalah static site generator atau lebih kita kenal sebagai SSG,yang mendukung projek blog maupun projek pribadi.

### Apa itu SSG(Static site generator) ?
Static site generator adalah alat yang menghasilkan situs web HTML statis penuh berdasarkan data mentah dan satu set template.
Ada banyak sekali SSG tidah hanya Jekyll saja 
ada hugo, ghost, gatsby, nextjs, nuxt, hexo dan masih banyak lagi jika kalian ingin mengetahui pebih banyak bisa ke situs [Jamstack.org](https://jamstack.org/generators).
### Kenapa Jekyll ?
Kenapa jekyll? Karena ringan cepat, Static site generator yang saya pertama kali tau adalah hugo dan saya dulu menggunakannya ddengan lama sampai akhirnya bertemu dengan Jekyll.
Cuma setelah hugo tidak langsung ke jekyll dulu sempat coba hexo, ghost dan ada lagi tapi lupa wkwkwk.
Selain cepat juga ekonomis bahkan bisa menggunakannya dg gratis.
Kita bisa menggunakan untuk hosting grqtis dari github maupun gitlab.

### Kenapa bukan Wordpess atau Blogger ?
Kenapa bukan wordpress atau blogger adalah karena di rumah saya sinyalnya jelek jadi kita kalau mau menulis atrikel harus terhubung ke internet, sementara jekyll buka terminal langsung bisa nulis ga harus terhubung ke internet.<br>
Dan kalau pakai wordpress tentu saja harus menyewa hosting untuk server, bagi saya sendiri kalau masih untuk sekedar nulis kaya gini cari yang gratis" aja wkwkwk.<br>
Sebenarnya bisa saja pakai Blogger bahkan saya dulu pernah pakai blogger dan udah sempat sampai di monetise tapi karena apa gitu akun dinonaktifkan jadi ya sekarang mau pake jekyll dulu.

## Gimana cara pasang jekyll ?
Cara meng-install jekyll sangat mudah dan tersedia untuk multi platfrom dari Windows, linux maupun MacOs.<br>
Cara install di linux seperti berikut:
##### Ubuntu
```html
sudo apt install ruby-full build-essential zlib1g-dev
```
Jika sudah install Ruby sekarang install Jekyllnya.
```
gem install jekyll bundler
```
Jika sudah selesai coba untuk memastikan jekyll sudah ter-instal apa belum masukkan perintah
```
jekyll -version
jekyll 4.3.3
```
Jika berhasil maka akan tampul seperti berikut.
// {% image https://i.postimg.cc/3NyRdrBs/IMG-20240404-151905.jpg | Jekyll-version %}

Jika sudah seperti itu artinya jekyll sudah ter-instal dan sekarang sudah bisa
membuaat sitenya,untuk membuatnya cukup masukkan perintah berikut.
```
jekyll new blog 
```
> blog adalah nama folder projek kita ini bisa sesuai kalian aja.

Jika sudah selesai generatenya sekarang kita masuk ke folder projek kita dengan
perintah.
```
cd blog
```
> blog diganti sesuai tadi kalian buatnya dengan nama apa.

Sekarang kita sudah menjalankannya di browser kita dengan perintah
```
jekyll server
```
<br>
![jekyll run](/assets/img/post/jekyll2.jpg)
Kemuadian ke browser dan masukkan alamat url **localhost:4000**.
![jekyll post](/assets/img/post/jekyll3.png)

Setelah ini kita nanti akan coba menambahkan tema ke jekyll supaya lebih enak di
pandang.