---
title: 'Menambahkan Tema di Jekyll?'
layout: post
date: '2024-04-06 07:35:33'
tags:
- jekyll
categories:
- Jekyll
description: 'Bagaimana menambahkan tema di jekyll?'
---
### Menambahkan Tema ke Jekyll 
Sebelumnya kita sufah berhasil membuat sebuah Site sederhana menggunakan jekyll,
sekarang kita akan coba menambahkan sebuah tema ke dalam site kita yang sudah
kita buat sebelumnyab. <br>
#### Kenapa Menambahkan Tema?
Kenapa kita harus menambahkan tema ke dalam site kita? sebenernya ga harus juga
bagi yang pengen aja, Karena supaya site kita lebih cantik nan indah untuk
dipandang mata, karena ada banyak sekali tema yang sudah siap pakai kalian bisa
memilihnya sendiri. <br>
#### Dimana Kita Bisa menemukan Tema Jekyll
##### jekyllthemes.org
Di [jekyllthemes.org](https://jekyllthemes.org) kita bisa menemukan banyak
sekali tema jekyll yg sudah tinggal pakai,
untuk pemasangannya biasanya sudah ada pada halaman temanya itu sendiri tinggal
membacannya di post halaman temanya.
##### jekyllthemes.io/free
Di [jekyllthemes.io/free](https://jekyllthemes.io/free) juga tersedia banyak
tema yg bagus bagus kita tinggal memilih sesuai keinginan kita.

### Cara Memasangnya
